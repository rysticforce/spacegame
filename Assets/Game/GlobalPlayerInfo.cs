﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GlobalPlayerInfo
{
    public static Sector[,] mainMap;
    public static List<Faction> factions;

    //Zooming on the shard map.
    public static Sector zoomedSector = null;

    public static Shard zoomedShard = null;

}
