﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Faction", menuName = "FactionTypes")]
public class FactionTypes : ScriptableObject
{
    public string factionName;

    public RaceTypes race;

    public Color color;

    public ShipTypes explorer;
    public int baseExplorerLimit;
    public int sectorsToNewExplorer;

    public ShipTypes flagShip;

    public ShipTypes cargo;
    public int baseCargoLimit;
    public int sectorsToNewCargo;

    public ShipTypes merchantShip;

    public ShipTypes colony;
    public int baseColonyLimit;
    public int sectorsToNewColony;

    public List<ShipTypes> defensiveShips;

    public List<ShipTypes> offensiveShips;

    public OrbitalBuildingTypes shardColonizationBuilding;

}