﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "New Orbital Building", menuName = "OrbitalBuildingTypes")]
public class OrbitalBuildingTypes : ScriptableObject
{
    public string buildingName;

    public int capacity;

    public List<OrbitalBuildingTypes> enabledOrbitalBuildings;

    public bool enablesConstruction;
    public bool canProduceDefensive;
    public bool canProduceOffensive;
    public bool canProduceFlagship;
    public bool canProduceCargo;
    public bool canProduceTrader;
}
