﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

[CreateAssetMenu(fileName = "New Ship", menuName = "ShipTypes")]
public class ShipTypes : ScriptableObject
{
    public enum ShipClassification
    { 
        EXPLORER,
        COLONY,
        CARGO,
        NONE
    }

    public ShipClassification shipClass;

    public int baseConstructionTime;

    public int baseMoveSpeed;
}
