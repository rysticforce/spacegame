﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Race", menuName = "RaceTypes")]
public class RaceTypes : ScriptableObject
{
    public Category favoredShardCategory;

    public string raceName;
}
