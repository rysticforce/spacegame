﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Trait", menuName = "TraitTypes")]
public class TraitTypes : ScriptableObject
{
    public string traitName;

    public int inheritanceChance;

    public List<OrbitalBuildingTypes> allowedOrbitalBuildings;

    public List<SurfaceBuildingTypes> allowedSurfaceBuildings;
}
