﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static AbstractMovementStrategy;
using static MovementStrategyFactory;
using static ShipTypes;

public class Faction
{
    private readonly static System.Random random = new System.Random();


    private readonly FactionTypes factionType;

    private readonly ShipManager shipManager;
    private readonly ExpansionManager expansionManager;
    private readonly ConstructionManager constructionManager;

    private readonly List<MovementStrategies> movementStrategies;

    private Color color;

    public Faction(FactionTypes factionType)
    {
        this.factionType = factionType;

        bool isSeizer = factionType.race.raceName.Equals("seizer");
        color = new Color(isSeizer ? (float) random.NextDouble() + .4f : 0f, (float) random.NextDouble(), isSeizer ? 0f : (float) random.NextDouble() + .4f);

        shipManager = new ShipManager(this, this.factionType);
        expansionManager = new ExpansionManager(this, this.factionType);
        constructionManager = new ConstructionManager(this, this.factionType);

        movementStrategies = new List<MovementStrategies>
        {
            MovementStrategies.WANDER
        };

    }

    public Trader CreateTraderUnit()
    {
        // TODO expand on this depending on the faction;
        Trader trader = new Trader();

        MovementStrategies moveStrategy = movementStrategies[random.Next(movementStrategies.Count)];

        trader.SetMovementStrategy(MovementStrategyFactory.CreateInstance(moveStrategy, trader));
        trader.SetRace(factionType.race)
            ;
        return trader;
    }

    public Category GetRacialFavoredShard()
    {
        return factionType.race.favoredShardCategory;
    }

    // Maybe pass in managers other managers need so I can delete these methods?

    public ShipManager GetShipManager()
    {
        return shipManager;
    }

    public ExpansionManager GetExpansionManager()
    {
        return expansionManager;
    }

    public ConstructionManager GetConstructionManager()
    {
        return constructionManager;
    }

    public Color GetColor()
    {
        return color;
    }
}
