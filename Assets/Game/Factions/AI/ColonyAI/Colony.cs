﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static ShipTypes;

public class Colony
{
    private readonly Faction faction;
    private readonly Sector originalSector;

    private readonly List<ShardGovernorAI> shardGovernors;
    private readonly List<OrbitalBuilding> orbitalBuildings;
    private readonly List<Ship> defensiveShips;
    private readonly List<Ship> offensiveShips;

    private readonly Dictionary<string, int> resourceStorage;

    private int capacity;
    private int remainingConstructionTime;
    private ShipTypes shipUnderConstruction;

    public Colony(Faction faction, Sector originalSector)
    {
        this.faction = faction;
        this.originalSector = originalSector;
        this.capacity = 0;
        this.remainingConstructionTime = 0;

        this.orbitalBuildings = new List<OrbitalBuilding>();
        this.defensiveShips = new List<Ship>();
        this.offensiveShips = new List<Ship>();
        this.shardGovernors = new List<ShardGovernorAI>();
        this.resourceStorage = new Dictionary<string, int>();

        foreach (Shard shard in originalSector.GetShards())
        {
            shardGovernors.Add(new ShardGovernorAI(shard));
        }

        originalSector.ColonizeSector(this);
    }

    public void DoIncome()
    {
        foreach (ShardGovernorAI governor in shardGovernors)
        {
            if (capacity == 0)
            {
                break;
            }
            capacity = governor.DoIncomeAndReturnCapacity(resourceStorage, capacity);
        }
    }

    public Sector GetOriginalSector()
    {
        return originalSector;
    }

    public void SetConstruction(ShipTypes newShipType)
    {
        // by this point, all resources should be consumed. None of the resource-consuming logic should be in here
        shipUnderConstruction = newShipType;
        remainingConstructionTime = newShipType.baseConstructionTime;
    }

    public bool IsConstructing()
    {
        return remainingConstructionTime > 0;
    }

    public void Construct()
    {
        remainingConstructionTime--;
        if (remainingConstructionTime == 0)
        {
            faction.GetShipManager().AddNewShip(shipUnderConstruction, originalSector);
            shipUnderConstruction = null;
        }
    }

    public bool CanProduceDefensive()
    {
        if (defensiveShips.Count >= orbitalBuildings.Count)
            return false;

        foreach (OrbitalBuilding building in orbitalBuildings)
        {
            if (building.CanProduceDefensive())
                return true;
        }
        return false;
    }

    public bool CanProduceOffensive()
    {
        if (offensiveShips.Count >= 1)
            return false;

        foreach (OrbitalBuilding building in orbitalBuildings)
        {
            if (building.CanProduceOffensive())
                return true;
        }
        return false;
    }


    public bool CanProduceFlagship()
    {
        foreach (OrbitalBuilding building in orbitalBuildings)
        {
            if (building.CanProduceFlagship())
                return true;
        }
        return false;
    }

    public bool CanProduceCargo()
    {
        foreach (OrbitalBuilding building in orbitalBuildings)
        {
            if (building.CanProduceCargo())
                return true;
        }
        return false;
    }

    public bool CanProduceTrader()
    {
        foreach (OrbitalBuilding building in orbitalBuildings)
        {
            if (building.CanProduceTrader())
                return true;
        }
        return false;
    }

    public bool CanProduceOrbitalBuilding()
    {
        return false; //orbitalBuildings.Count <= originalSector.GetShards().Length;
    }

    public void AddOrbitalBuilding(OrbitalBuildingTypes buildingType)
    {
        orbitalBuildings.Add(new OrbitalBuilding(buildingType));
    }

    public ShipClassification GetConstructingShipClassification()
    {
        if (IsConstructing())
            return shipUnderConstruction.shipClass;
        else
            return ShipClassification.NONE;
    }

    //-> Responsible for alerting Faction that a new Flagship has been created.
    //  -> Responsible for doing income from shard governor.
    //-> Responsible for letting shard governors know what buildings they are allowed to construct, besides the defaults.
    //  -> Responsible for starting construction on ships.
    //-> Responsible for starting construction on orbital structures.
    //  -> Responsible for tracking ship-building progress
    // ??? -> Responsible for responding to requests of other sectors
}