﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrbitalBuilding
{
    private readonly OrbitalBuildingTypes type;

    public OrbitalBuilding(OrbitalBuildingTypes type)
    {
        this.type = type;
    }

    public int GetCapacity()
    {
        return type.capacity;
    }

    public bool CanProduceDefensive()
    {
        return type.canProduceDefensive; 
    }

    public bool CanProduceOffensive()
    {
        return type.canProduceOffensive;
    }

    public bool CanProduceFlagship()
    {
        return type.canProduceFlagship;
    }

    public bool CanProduceCargo()
    {
        return type.canProduceCargo; 
    }

    public bool CanProduceTrader()
    {
        return type.canProduceTrader; 
    }

}