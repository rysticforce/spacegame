﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColonizationBlock
{
    private readonly List<SurfaceCell> cells;
    private readonly HashSet<ColonizationBlock> adjacentBlocks;
    private readonly int length;

    public ColonizationBlock(int length)
    {
        this.cells = new List<SurfaceCell>();
        this.adjacentBlocks = new HashSet<ColonizationBlock>();
        this.length = length;
    }

    public void AddSurfaceCell(SurfaceCell surfaceCell)
    {
        cells.Add(surfaceCell);
    }

    public List<SurfaceCell> GetSurfaceCells()
    {
        return cells;
    }

    public int GetLength()
    {
        return length;
    }

    public void AddAdjacentBlock(ColonizationBlock colonizationBlock)
    {
        adjacentBlocks.Add(colonizationBlock);
    }
}
