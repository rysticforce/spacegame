﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColonizationProfile
{
    private readonly Dictionary<Terrain, HashSet<ColonizationBlock>> blockMap; 

    public ColonizationProfile(Shard shard)
    {
        blockMap = new Dictionary<Terrain, HashSet<ColonizationBlock>>();

        GenerateBlocks(shard, true);
        GenerateBlocks(shard, false);

        SetAdjacentColonizationBlocks();
    }

    public void SetAdjacentColonizationBlocks()
    {
        foreach (Terrain terrain in blockMap.Keys)
        {
            foreach (ColonizationBlock block in blockMap[terrain])
            {
                foreach (SurfaceCell cell in block.GetSurfaceCells())
                {
                    Debug.Log(cell.GetAdjacentCells().Count);
                    foreach (SurfaceCell adjacentCell in cell.GetAdjacentCells())
                    {
                        block.AddAdjacentBlock(adjacentCell.colonizationBlock); 
                    }
                }
            }
        }
    }

    private void GenerateBlocks(Shard shard, bool breakSmallerBlocks)
    {
        try
        {
            if (shard.GetEnvironment() == null)
                EnvironmentGenerator.CreateEnvironment(shard);
        }
        catch (System.Exception e)
        {
            return;
        }
        SurfaceCell[,] terrainGrid = shard.GetTerrainGrid();
        for (int i = 0; i < terrainGrid.GetLength(0); i++)
        {
            for (int j = 0; j < terrainGrid.GetLength(1); j++)
            {
                SurfaceCell startingCell = terrainGrid[i, j];

                if (!breakSmallerBlocks && startingCell.colonizationBlock != null)
                {
                    continue;
                }

                SurfaceCell current = startingCell;
                int maxWidth = 0;

                while (current.terrain == startingCell.terrain)
                {
                    maxWidth++;
                    if (current.X < terrainGrid.GetLength(0) - 1)
                        current = terrainGrid[current.X + 1, current.Y];
                    else
                        break;
                }

                current = startingCell;
                int maxHeight = 0;

                while (current.terrain == startingCell.terrain)
                {
                    maxHeight++;
                    if (current.Y < terrainGrid.GetLength(1) - 1)
                        current = terrainGrid[current.X, current.Y + 1];
                    else
                        break;
                }

                int greatestPossibleLength = Mathf.Min(maxWidth, maxHeight);

                int realGPL = 0;
                bool failed = false;
                for (;realGPL < greatestPossibleLength; realGPL++)
                {
                    for (int w = 0; w < realGPL; w++)
                    {
                        for (int h = 0; h < realGPL; h++)
                        {
                            current = terrainGrid[w + startingCell.X, h + startingCell.Y];
                            failed |= (current.terrain != startingCell.terrain || (!breakSmallerBlocks && current.colonizationBlock != null));
                        }
                    }
                    if (failed)
                        break;
                }

                ColonizationBlock block = new ColonizationBlock(realGPL);
                List<SurfaceCell> cellsInBlock = new List<SurfaceCell>();
                for (int w = 0; w < realGPL; w++)
                {
                    for (int h = 0; h < realGPL; h++)
                    {
                        cellsInBlock.Add(terrainGrid[w + startingCell.X, h + startingCell.Y]);
                    }
                }


                if (breakSmallerBlocks)
                {
                    failed = false;
                    foreach (SurfaceCell cell in cellsInBlock)
                    {
                        if (cell.colonizationBlock != null)
                        {
                            failed |= cell.colonizationBlock.GetLength() >= realGPL;
                            if (failed)
                                break;
                        } 
                    }

                    if (failed)
                        continue;

                    foreach (SurfaceCell cell in cellsInBlock)
                    {
                        if (cell.colonizationBlock != null)
                        {
                            blockMap[current.terrain].Remove(cell.colonizationBlock);
                        }
                    }
                }
                foreach (SurfaceCell cell in cellsInBlock)
                {
                    block.AddSurfaceCell(cell);
                    cell.SetColonizationBlock(block);
                }

                HashSet<ColonizationBlock> blockQueue;

                if (blockMap.ContainsKey(startingCell.terrain))
                    blockQueue = blockMap[startingCell.terrain];
                else
                {
                    blockQueue = new HashSet<ColonizationBlock>();
                    blockMap[startingCell.terrain] = blockQueue;
                }
                blockQueue.Add(block);
            }
        }
    }

    private void GetBlockSize(Terrain[,] terrainGrid, int i, int j, int currSize)
    {

    }


}
