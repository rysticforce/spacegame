﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShardGovernorAI
{
    private readonly Shard homeShard;

    private readonly ColonizationProfile profile;

    public ShardGovernorAI(Shard homeShard)
    {
        this.homeShard = homeShard;
        profile = new ColonizationProfile(this.homeShard);
    }

    public void DoBuild()
    {
         
    }

    public int DoIncomeAndReturnCapacity(Dictionary<string, int> resourceStorage, int capacity)
    {
        int newCapacity = capacity;
        //TODO Actually harvest resources
        return newCapacity;
    }
}
