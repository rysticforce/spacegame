﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExpansionManager
{
    private readonly Faction faction;
    private readonly FactionTypes factionType;

    private readonly HashSet<Sector> colonizedSectors;
    private readonly SectorSnapshot[,] sectorSnapshots;

    private Sector latestColonizedSector;

    private readonly PriorityQueue<SectorSnapshot> bestSectorsToColonize;

    private readonly static System.Random random = new System.Random();

    public ExpansionManager(Faction faction, FactionTypes factionType)
    {
        this.faction = faction;
        this.factionType = factionType;

        latestColonizedSector = null;

        colonizedSectors = new HashSet<Sector>();
        sectorSnapshots = new SectorSnapshot[GlobalPlayerInfo.mainMap.GetLength(0), GlobalPlayerInfo.mainMap.GetLength(1)];
        bestSectorsToColonize = new PriorityQueue<SectorSnapshot>();
    }

    // Colonization

    public void AddColonizedSector(Sector sector)
    {
        Colony colony = new Colony(faction, sector);
        sector.ColonizeSector(colony);
        colonizedSectors.Add(sector);
        latestColonizedSector = sector;
    }

    public SectorSnapshot GetNextSectorSnapshotToColonize()
    {
        return bestSectorsToColonize.Pop();
    }

    public bool HasSectorToColonize()
    {
        return bestSectorsToColonize.HasNext();
    }

    public HashSet<Sector> GetColonizedSectors()
    {
        return colonizedSectors;
    }

    // Sectors

    public bool ControlsSector(Sector sector)
    {
        return colonizedSectors.Contains(sector);
    }

    public Sector GetLatestColonizedSector()
    {
        return latestColonizedSector; 
    }

    public void UpdateKnownSectors(Dictionary<Tuple<int, int>, SectorSnapshot> exploredSectors)
    {
        foreach (Tuple<int, int> coord in exploredSectors.Keys)
        {
            SectorSnapshot oldSnapshot = sectorSnapshots[coord.Item1, coord.Item2];
            if (oldSnapshot == null || oldSnapshot.GetTurnCreated() < exploredSectors[coord].GetTurnCreated())
            {
                sectorSnapshots[coord.Item1, coord.Item2] = exploredSectors[coord];
                if (!bestSectorsToColonize.HasElement(exploredSectors[coord]) && exploredSectors[coord].GetScore() > 0)
                {
                    bestSectorsToColonize.Push(exploredSectors[coord], exploredSectors[coord].GetScore());
                }

            }
        }
    }

    //util
    public Sector FindClosestOwnedSector(Sector startingSector)
    {
        int bestScore = 0;
        List<Sector> bestSectors = new List<Sector>();

        foreach (Sector sector in colonizedSectors)
        {
            int score = Math.Abs(sector.X - startingSector.X) + Math.Abs(sector.Y - startingSector.Y);
            if (bestSectors.Count == 0 || score < bestScore)
            {
                bestSectors.Clear();
                bestSectors.Add(sector);

                bestScore = score;
            }
            else if (bestSectors.Count > 0 && score == bestScore)
            {
                bestSectors.Add(sector);
            }
        }

        return bestSectors[random.Next(bestSectors.Count)];
    }

    //util
    public Sector GetOldestSector()
    {
        long oldestSnapshotScore = 0;
        List<Sector> oldestSectors = new List<Sector>();

        foreach (SectorSnapshot sectorSnapshot in sectorSnapshots)
        {
            if (sectorSnapshot == null)
                continue;
            Sector sector = GlobalPlayerInfo.mainMap[sectorSnapshot.X, sectorSnapshot.Y];

            if (colonizedSectors.Contains(sector))
                continue;

            long score = GetTurnSnapshotCreated(sector.X, sector.Y);

            if (oldestSectors.Count == 0 || score < oldestSnapshotScore)
            {
                oldestSectors.Clear();
                oldestSectors.Add(sector);

                oldestSnapshotScore = score;
            }
            else if (oldestSectors.Count > 0 && score == oldestSnapshotScore)
            {
                oldestSectors.Add(sector);
            }
        }

        if (oldestSectors.Count == 0)
            return null;

        return oldestSectors[random.Next(oldestSectors.Count)]; ;
    }


    // Sector Snapshots

    public SectorSnapshot CreateSectorSnapshot(Sector sector)
    {
        SectorSnapshot result = new SectorSnapshot(sector.X, sector.Y);

        int score = 0;

        foreach (Shard shard in sector.GetShards())
        {
            if (shard.GetCategory() == faction.GetRacialFavoredShard())
            {
                score += shard.GetSize();
            }
        }
        result.SetScore(score);
        return result;
    }

    //util
    public long GetTurnSnapshotCreated(int x, int y)
    {
        if (sectorSnapshots[x, y] == null)
        {
            return -1;
        }
        return sectorSnapshots[x, y].GetTurnCreated();
    }

 }