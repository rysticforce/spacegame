﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static ShipTypes;

public class ShipManager
{
    private readonly Faction faction;
    private readonly FactionTypes factionType;

    private readonly Dictionary<ShipClassification, HashSet<Ship>> unassignedShips;
    private readonly Dictionary<ShipClassification, HashSet<Ship>> assignedShips;

    public ShipManager(Faction faction, FactionTypes factionType)
    {
        this.faction = faction;
        this.factionType = factionType;

        unassignedShips = new Dictionary<ShipClassification, HashSet<Ship>>();
        assignedShips = new Dictionary<ShipClassification, HashSet<Ship>>();
    }

    public void AddNewShip(ShipTypes shipType, Sector sector)
    {
        Ship ship = new Ship(faction, sector, shipType);
        ShipClassification shipClass = ship.GetClassification();
        if (!unassignedShips.ContainsKey(shipClass))
        {
            unassignedShips[shipClass] = new HashSet<Ship>();
        }
        unassignedShips[shipClass].Add(ship);
    }

    public void MoveShips()
    {
        foreach (ShipClassification shipClass in assignedShips.Keys)
        {
            List<Ship> finishedOrders = new List<Ship>();
            foreach (Ship ship in assignedShips[shipClass])
            {
                if (ship.GetOrder().IsFailed())
                {
                    ship.SetOrder(null);
                    finishedOrders.Add(ship);
                    continue;
                }

                ship.GetOrder().Move();

                if (ship.GetOrder().IsCompleted())
                {
                    ship.SetOrder(null);
                    finishedOrders.Add(ship);
                }
                else
                    ship.GetOrder().NextMove();

            }

            foreach (Ship ship in finishedOrders)
            {
                assignedShips[shipClass].Remove(ship);
                unassignedShips[shipClass].Add(ship);
            }
        }
    }

    public void AssignOrders()
    {
        ExpansionManager expansionManager = faction.GetExpansionManager();

        foreach (ShipClassification shipClass in unassignedShips.Keys)
        {
            List<Ship> startedOrders = new List<Ship>();

            if (shipClass == ShipClassification.EXPLORER)
            {
                foreach (Ship ship in unassignedShips[ShipClassification.EXPLORER])
                {
                    if (ship.GetExploredSectorCount() > 2)
                        ship.SetOrder(new RallyOrder(ship, expansionManager.FindClosestOwnedSector(ship.GetSector())));
                    else if (expansionManager.ControlsSector(ship.GetSector()))
                    {
                        Sector oldestSector = expansionManager.GetOldestSector();
                        if (oldestSector == null)
                            ship.SetOrder(new ExploreOrder(faction, ship));
                        else
                            ship.SetOrder(new RallyOrder(ship, oldestSector));
                    }
                    else
                    {
                        ship.SetOrder(new ExploreOrder(faction, ship));
                    }
                    startedOrders.Add(ship);
                }
            }

            if (shipClass == ShipClassification.COLONY)
            {
                foreach (Ship ship in unassignedShips[ShipClassification.COLONY])
                {
                    SectorSnapshot tss = faction.GetExpansionManager().GetNextSectorSnapshotToColonize();
                    if (tss == null)
                    {
                        ship.SetOrder(new RallyOrder(ship, expansionManager.FindClosestOwnedSector(ship.GetSector())));
                        startedOrders.Add(ship);
                        continue;
                    }
                    Sector targetSector = GlobalPlayerInfo.mainMap[tss.X, tss.Y];
                    if (targetSector == null)
                        break;
                    ship.SetOrder(new ColonizeOrder(faction, ship, targetSector));
                    startedOrders.Add(ship);
                }
            }

            foreach (Ship ship in startedOrders)
            {
                ship.GetOrder().NextMove();
                unassignedShips[shipClass].Remove(ship);
                if (!assignedShips.ContainsKey(shipClass))
                {
                    assignedShips[shipClass] = new HashSet<Ship>();
                }

                assignedShips[shipClass].Add(ship);
            }
        }

    }

    // getting the best ship

    public ShipTypes GetBestExplorer()
    {
        return factionType.explorer;
    }

    public ShipTypes GetBestCargo()
    {
        return factionType.cargo;
    }

    public ShipTypes GetBestColony()
    {
        return factionType.colony;
    }

    // deleting dead ships

    public void RemoveMarkedForDestruction()
    {
        foreach (ShipClassification shipClass in unassignedShips.Keys)
        {
            List<Ship> markedForDestruction = new List<Ship>();
            foreach (Ship ship in unassignedShips[shipClass])
            {
                if (ship.IsMarkedForDestruction())
                    markedForDestruction.Add(ship);
            }
            foreach (Ship ship in markedForDestruction)
            {
                unassignedShips[shipClass].Remove(ship);
            }
        }

        foreach (ShipClassification shipClass in assignedShips.Keys)
        {
            List<Ship> markedForDestruction = new List<Ship>();
            foreach (Ship ship in assignedShips[shipClass])
            {
                if (ship.IsMarkedForDestruction())
                    markedForDestruction.Add(ship);
            }
            foreach (Ship ship in markedForDestruction)
            {
                assignedShips[shipClass].Remove(ship);
            }
        }
    }

    public List<Ship> GetAllShips()
    {
        List<Ship> ships = new List<Ship>();
        foreach (ShipClassification shipClass in unassignedShips.Keys)
        {
            foreach (Ship ship in unassignedShips[shipClass])
            {
                ships.Add(ship);
            }
        }
        foreach (ShipClassification shipClass in assignedShips.Keys)
        {
            foreach (Ship ship in assignedShips[shipClass])
            {
                ships.Add(ship);
            }
        }
        return ships;
    }

    public int GetShipTypeCount(ShipClassification classification)
    {
        if (!unassignedShips.ContainsKey(classification))
            unassignedShips[classification] = new HashSet<Ship>();
        if (!assignedShips.ContainsKey(classification))
            assignedShips[classification] = new HashSet<Ship>();
        return unassignedShips[classification].Count + assignedShips[classification].Count;
    }
}