﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static ShipTypes;

public class ConstructionManager
{
    private readonly Faction faction;
    private readonly FactionTypes factionType;

    private readonly Dictionary<Sector, ConstructionRequest> pendingRequests;

    private readonly Dictionary<ShipClassification, int> underConstructionCount;

    public ConstructionManager(Faction faction, FactionTypes factionType)
    {
        this.faction = faction;
        this.factionType = factionType;

        pendingRequests = new Dictionary<Sector, ConstructionRequest>();
        underConstructionCount = new Dictionary<ShipClassification, int>();
    }

    public void DoConstruction()
    {
        ShipManager shipManager = faction.GetShipManager();
        List<Sector> completedSectors = new List<Sector>();
        foreach (Sector sector in pendingRequests.Keys)
        {
            ConstructionRequest request = pendingRequests[sector];
            // loop through cargo ships, assigning one to each pending request. if cargo ships > pending requests, start from the
            // beginning of the list and go through until all cargo are assigned.
            if (request.IsComplete())
            {
                shipManager.AddNewShip(request.GetShipType(), sector);
                completedSectors.Add(sector);
            }
            else
            {
                request.ProgressConstruction();
            }
        }
        foreach (Sector sector in completedSectors)
        {
            pendingRequests.Remove(sector);
        }
    }

    public void AssignNewConstructions()
    {
        ConstructShipType(faction.GetExpansionManager().GetLatestColonizedSector(), faction.GetShipManager().GetBestColony(), factionType.baseColonyLimit, factionType.sectorsToNewColony);
        ConstructShipType(faction.GetExpansionManager().GetOldestSector(), faction.GetShipManager().GetBestExplorer(), factionType.baseExplorerLimit, factionType.sectorsToNewExplorer);
        ConstructShipType(null, faction.GetShipManager().GetBestCargo(), factionType.baseCargoLimit, factionType.sectorsToNewCargo);
    }

    private void ConstructShipType(Sector proximitySector, ShipTypes shipType, int baseLimit, int sectorsToNew)
    {
        ExpansionManager expansionManager = faction.GetExpansionManager();

        PriorityQueue<Sector> bestSectors = new PriorityQueue<Sector>();
        
        foreach (Sector sector in expansionManager.GetColonizedSectors())
        {
            if (pendingRequests.ContainsKey(sector))
                continue;

            int score = 0;
            if (proximitySector != null)
                score = 100 - (Math.Abs(sector.X - proximitySector.X) + Math.Abs(sector.Y - proximitySector.Y));
            bestSectors.Push(sector, score);
        }

        while (bestSectors.HasNext() && CanBuildShipClass(shipType.shipClass, baseLimit, sectorsToNew))
        {
            Sector bestSector = bestSectors.Pop();
            pendingRequests[bestSector] = new ConstructionRequest(bestSector, shipType);
        }
    }

    public bool CanBuildShipClass(ShipClassification classification, int baseLimit, int sectorsToNew)
    {
        if (!underConstructionCount.ContainsKey(classification))
            underConstructionCount[classification] = 0;
        int totalShipsOfType = underConstructionCount[classification] + faction.GetShipManager().GetShipTypeCount(classification);

        return totalShipsOfType < baseLimit + (faction.GetExpansionManager().GetColonizedSectors().Count / sectorsToNew);
    }
}
