﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExploreOrder : AbstractOrder
{
    private readonly static System.Random random = new System.Random();
    private readonly Faction faction;

    private Sector currentSector;
    private Sector nextSector;

    private int homeSectorX;
    private int homeSectorY;

    private readonly HashSet<Sector> allExploredSectors;
    private List<Sector> previousMoves;

    public ExploreOrder(Faction faction, Ship executingShip) : base(executingShip)
    {
        currentSector = executingShip.GetSector();

        this.faction = faction;

        this.homeSectorX = executingShip.GetSector().X;
        this.homeSectorY = executingShip.GetSector().Y;

        this.allExploredSectors = new HashSet<Sector>();
        this.previousMoves = new List<Sector>();
    }

    public override void NextMove()
    {
        int x = currentSector.X;
        int y = currentSector.Y;

        List<Sector> allMoves = new List<Sector>();
        List<Sector> possibleMoves = new List<Sector>();

        ExpansionManager manager = faction.GetExpansionManager();

        long lowestGameTurn = FactionAI.GetTotalGameTurns();

        if (x - 1 >= 0)
        {
            if (!TooFarFromHome(x - 1, y) && !executingShip.HasExplored(GlobalPlayerInfo.mainMap[x - 1, y]) && !manager.ControlsSector(GlobalPlayerInfo.mainMap[x - 1, y]))
            {
                if (manager.GetTurnSnapshotCreated(x - 1, y) < lowestGameTurn)
                {
                    possibleMoves.Clear();
                    lowestGameTurn = manager.GetTurnSnapshotCreated(x - 1, y);
                }
                possibleMoves.Add(GlobalPlayerInfo.mainMap[x - 1, y]);
            }
            allMoves.Add(GlobalPlayerInfo.mainMap[x - 1, y]);
        }

        if (x + 1 < GlobalPlayerInfo.mainMap.GetLength(0))
        {
            if (!TooFarFromHome(x + 1, y) && !executingShip.HasExplored(GlobalPlayerInfo.mainMap[x + 1, y]) && !manager.ControlsSector(GlobalPlayerInfo.mainMap[x + 1, y]))
            {
                if (manager.GetTurnSnapshotCreated(x + 1, y) < lowestGameTurn)
                {
                    possibleMoves.Clear();
                    lowestGameTurn = manager.GetTurnSnapshotCreated(x + 1, y);
                }
                possibleMoves.Add(GlobalPlayerInfo.mainMap[x + 1, y]);
            }
            allMoves.Add(GlobalPlayerInfo.mainMap[x + 1, y]);
        }

        if (y - 1 >= 0)
        {
            if (!TooFarFromHome(x, y - 1) && !executingShip.HasExplored(GlobalPlayerInfo.mainMap[x, y - 1]) && !manager.ControlsSector(GlobalPlayerInfo.mainMap[x, y - 1]))
            {
                if (manager.GetTurnSnapshotCreated(x, y - 1) < lowestGameTurn)
                {
                    possibleMoves.Clear();
                    lowestGameTurn = manager.GetTurnSnapshotCreated(x, y - 1);
                }
                possibleMoves.Add(GlobalPlayerInfo.mainMap[x, y - 1]);
            }
            allMoves.Add(GlobalPlayerInfo.mainMap[x, y - 1]);
        }

        if (y + 1 < GlobalPlayerInfo.mainMap.GetLength(1))
        {
            if (!TooFarFromHome(x, y + 1) && !executingShip.HasExplored(GlobalPlayerInfo.mainMap[x, y + 1]) && !manager.ControlsSector(GlobalPlayerInfo.mainMap[x, y + 1]))
            {
                if (manager.GetTurnSnapshotCreated(x, y + 1) < lowestGameTurn)
                {
                    possibleMoves.Clear();
                }
                possibleMoves.Add(GlobalPlayerInfo.mainMap[x, y + 1]);
            }
            allMoves.Add(GlobalPlayerInfo.mainMap[x, y + 1]);
        }

        if (possibleMoves.Count == 0)
        {
            homeSectorX = currentSector.X;
            homeSectorY = currentSector.Y;
            nextSector = allMoves[random.Next(allMoves.Count)];
        }
        else
        {
            nextSector = possibleMoves[random.Next(possibleMoves.Count)];
        }
        previousMoves = allMoves;
    }

    public override void Move()
    {
        currentSector = nextSector;
        nextSector = null;
        executingShip.SetSector(currentSector);

        allExploredSectors.Add(currentSector);

        completed |= executingShip.GetExploredSectorCount() >= 7 || (previousMoves.Count > 0 && allExploredSectors.IsProperSupersetOf(previousMoves));

    }

    private bool TooFarFromHome(int x, int y)
    {
        return Mathf.Abs(homeSectorX - x) > 2 || Mathf.Abs(homeSectorY - y) > 2;
    }
}
