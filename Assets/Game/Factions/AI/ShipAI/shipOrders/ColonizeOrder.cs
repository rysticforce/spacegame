﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColonizeOrder : RallyOrder
{
    private readonly Faction faction;

    public ColonizeOrder(Faction faction, Ship executingShip, Sector targetSector) : base(executingShip, targetSector)
    {
        this.faction = faction;
    }

    public override void NextMove()
    {
        base.NextMove();
        failed = targetSector.IsColonizedSector();
    }

    public override void Move()
    {
        base.Move();
        if (IsCompleted() && !currentSector.IsColonizedSector())
        {
            faction.GetExpansionManager().AddColonizedSector(currentSector);
            executingShip.MarkForDestruction();
        }
    }
}
