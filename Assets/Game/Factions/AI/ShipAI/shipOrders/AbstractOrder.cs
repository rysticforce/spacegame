﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AbstractOrder
{
    protected readonly Ship executingShip;

    protected bool completed;
    protected bool failed;

    protected AbstractOrder(Ship executingShip)
    {
        this.executingShip = executingShip;
    }

    public bool IsCompleted()
    {
        return completed;
    }

    public bool IsFailed()
    {
        return failed;
    }

    public abstract void NextMove();

    public abstract void Move();

}
