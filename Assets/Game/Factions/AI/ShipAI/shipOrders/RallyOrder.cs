﻿using UnityEngine;

public class RallyOrder : AbstractOrder
{
    protected readonly Sector targetSector;
    protected Sector currentSector;
    protected Sector nextSector;

    public RallyOrder(Ship executingShip, Sector targetSector) : base(executingShip)
    {
        this.targetSector = targetSector;
        currentSector = executingShip.GetSector();
    }

    public override void NextMove()
    {
        if (currentSector.X > targetSector.X)
            nextSector = GlobalPlayerInfo.mainMap[currentSector.X - 1, currentSector.Y];
        else if (currentSector.X < targetSector.X)
            nextSector = GlobalPlayerInfo.mainMap[currentSector.X + 1, currentSector.Y];
        else if (currentSector.Y > targetSector.Y)
            nextSector = GlobalPlayerInfo.mainMap[currentSector.X, currentSector.Y - 1];
        else if (currentSector.Y < targetSector.Y)
            nextSector = GlobalPlayerInfo.mainMap[currentSector.X, currentSector.Y + 1];
    }

    public override void Move()
    {
        if (failed)
            return;

        if (currentSector.X == targetSector.X && currentSector.Y == targetSector.Y)
        {
            executingShip.SetSector(targetSector);
            completed = true;
            return;
        }

        currentSector = nextSector;
        nextSector = null;
        executingShip.SetSector(currentSector);
    }
}