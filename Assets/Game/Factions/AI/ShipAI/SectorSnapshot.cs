﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SectorSnapshot
{
    public readonly int X;
    public readonly int Y;

    private readonly long turnCreated;
    private int score;

    private bool isColonizable;

    public SectorSnapshot(int x, int y)
    {
        this.X = x;
        this.Y = y;
        this.turnCreated = FactionAI.GetTotalGameTurns();
    }

    public void SetColonizable(bool colonizable)
    {
        isColonizable = colonizable;
    }

    public bool GetColonizable()
    {
        return isColonizable;
    }

    public void SetScore(int score)
    {
        this.score = score;
    }

    public int GetScore()
    {
        return score;
    }

    public long GetTurnCreated()
    {
        return turnCreated;
    }
}