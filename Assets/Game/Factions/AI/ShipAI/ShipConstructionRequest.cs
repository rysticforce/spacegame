﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConstructionRequest
{
    private readonly Sector sector;
    private readonly ShipTypes shipType;

    private int turnsToCompletion;

    public ConstructionRequest(Sector sector, ShipTypes shipType)
    {
        this.sector = sector;
        this.shipType = shipType;

        turnsToCompletion = this.shipType.baseConstructionTime;
    }

    public Sector GetSector()
    {
        return sector;
    }

     public ShipTypes GetShipType()
    {
        return shipType; 
    }

    public bool IsComplete()
    {
        return turnsToCompletion <= 0;
    }

    public void ProgressConstruction()
    {
        turnsToCompletion--;
    }
}
