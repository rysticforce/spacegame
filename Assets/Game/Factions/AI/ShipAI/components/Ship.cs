﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static ShipTypes;

public class Ship
{
    private readonly Faction faction;

    private readonly ShipTypes shipType;
    private Sector currentSector;
    private AbstractOrder order;

    private readonly Dictionary<Tuple<int,int>, SectorSnapshot> exploredSectors;

    private bool markedForDestruction;

    public Ship(Faction faction, Sector startingSector, ShipTypes shipType)
    {
        this.faction = faction;
        this.currentSector = startingSector;
        this.shipType = shipType;
        markedForDestruction = false;
        exploredSectors = new Dictionary<Tuple<int, int>, SectorSnapshot> ();
    }

    public ShipClassification GetClassification()
    {
        return shipType.shipClass;
    }

    public Sector GetSector()
    {
        return currentSector;
    }

    internal bool IsInCombat()
    {
        throw new NotImplementedException();
    }

    public AbstractOrder GetOrder()
    {
        return order; 
    }

    public void SetOrder(AbstractOrder order)
    {
        this.order = order;
    }

    public void CancelOrder()
    {
        this.order = null;
    }

    public bool IsMoving()
    {
        return order != null;
    }

    public void SetSector(Sector sector)
    {
        currentSector = sector;
        ExpansionManager manager = faction.GetExpansionManager();
        if (manager.ControlsSector(sector))
        {
            manager.UpdateKnownSectors(exploredSectors);
            exploredSectors.Clear();
        }
        else
        {
            exploredSectors[new Tuple<int,int>(sector.X, sector.Y)] = manager.CreateSectorSnapshot(sector);
        }
    }

    public int GetExploredSectorCount()
    {
        return exploredSectors.Keys.Count;
    }

    public void MarkForDestruction()
    {
        markedForDestruction = true; 
    }

    public bool IsMarkedForDestruction()
    {
        return markedForDestruction;
    }

    internal bool HasExplored(Sector sector)
    {
        return exploredSectors.ContainsKey(new Tuple<int, int>(sector.X, sector.Y));
    }
}
