﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PriorityQueue<T>
{
    private readonly List<Node> pQueue;
    private readonly HashSet<T> containedElements;

    public PriorityQueue()
    {
        this.pQueue = new List<Node>();
        this.containedElements = new HashSet<T>();
    }

    public void Push(T data, int value)
    {
        if (HasElement(data))
        {
            throw new Exception("Duplicate entry into Priority Queue");
        }

        Node node = new Node(data, value);
        pQueue.Add(node);
        int index = pQueue.Count - 1;
        while (index > 0 && pQueue[index].value > pQueue[index/2].value)
        {
            Swap(index, index / 2);
            index /= 2;
        }

        containedElements.Add(data);
    }

    public T Pop()
    {
        if (pQueue.Count == 0)
            return default;
        Node result = pQueue[0];
        pQueue.RemoveAt(0);

        Sink(0);

        containedElements.Remove(result.data);

        return result.data;
    }

    private void Sink(int index)
    {
        bool hasRight = false;

        int lIndex = (index * 2) + 1;
        int rIndex = (index * 2) + 2;

        int bestValue;
        int bestIndex;
        if (lIndex <= pQueue.Count - 1)
        {
            bestValue = pQueue[lIndex].value;
            bestIndex = lIndex;

            if (rIndex <= pQueue.Count - 1)
            {
                hasRight = true;
                if (pQueue[rIndex].value > bestValue)
                {
                    bestValue = pQueue[rIndex].value;
                    bestIndex = rIndex;
                }
            }
            if (bestValue > pQueue[index].value)
            {
                Swap(index, bestIndex);
            }

            Sink(lIndex);
            if (hasRight)
                Sink(rIndex);
        }
    }

    private void Swap(int firstIndex, int secondIndex)
    {
        Node temp = pQueue[secondIndex];
        pQueue[secondIndex] = pQueue[firstIndex];
        pQueue[firstIndex] = temp;
    }

    public bool HasNext()
    {
        return pQueue.Count > 0;
    }

    public bool HasElement(T data)
    {
        return containedElements.Contains(data);
    }

    public void Print()
    {
        string result = "";
        foreach (Node node in pQueue)
        {
            result += " " + node.value;
        }
        Debug.Log(result); 
    }


    private class Node
    {
        public readonly T data;
        public readonly int value;

        public Node(T data, int value)
        {
            this.data = data;
            this.value = value; 
        }
    }
}
