﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AbstractMovementStrategy
{
    protected readonly Trader trader;
    protected Room nextRoom;

    protected AbstractMovementStrategy(Trader trader)
    {
        this.trader = trader;
    }

    protected bool IsNextRoomValid()
    {
        if (nextRoom == null)
            return false;
            
        return nextRoom.IsRoomAdjacent(trader.GetCurrentRoom());
    }

    public abstract void Move();

    public abstract Room DetermineNextRoom();
}
