﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MovementStrategyFactory
{
    public static readonly Dictionary<MovementStrategies, Type> strategyMap = new Dictionary<MovementStrategies, Type>();

    public enum MovementStrategies
    {
        WANDER,
        LOST
    }

    static MovementStrategyFactory()
    {
        strategyMap[MovementStrategies.WANDER] = typeof(WanderMovement);
    }

    public static AbstractMovementStrategy CreateInstance(MovementStrategies movementStrategy, Trader trader)
    {
        return (AbstractMovementStrategy)Activator.CreateInstance(strategyMap[movementStrategy], trader);
    }
}
