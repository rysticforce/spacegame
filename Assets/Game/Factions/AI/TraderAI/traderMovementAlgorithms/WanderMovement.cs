﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static MovementStrategyFactory;

public class WanderMovement : AbstractMovementStrategy
{
    public static readonly System.Random random = new System.Random();
    private static readonly int LOST_MOVEMENT_CHANCE = 25;

    private readonly Stack<Room> traversedRooms;
    private readonly HashSet<Room> visitedRooms;

    bool isReturning;

    public WanderMovement(Trader trader) : base(trader)
    {
        isReturning = false;
        traversedRooms = new Stack<Room>();
        visitedRooms = new HashSet<Room>();
    }

    public override void Move()
    {
        if (!IsNextRoomValid())
        {
            nextRoom = DetermineNextRoom();
            if (nextRoom == null)
                return;
        }

        Debug.Log("Moved " + trader.GetCurrentRoom().GetDirectionOfAdjacentRoom(nextRoom));

        visitedRooms.Add(trader.GetCurrentRoom());
        traversedRooms.Push(trader.GetCurrentRoom());

        trader.SetCurrentRoom(nextRoom);


        //TODO put physical movement here
        nextRoom = DetermineNextRoom();
    }


    public override Room DetermineNextRoom()
    {
        Room currentRoom = trader.GetCurrentRoom();
        Room possibleNextRoom = null;
        if (isReturning)
        {
            if (traversedRooms.Count > 0)
            {
                possibleNextRoom = traversedRooms.Pop();
                Room.Direction dir = currentRoom.GetDirectionOfAdjacentRoom(nextRoom);
                if (dir == Room.Direction.NOT_ADJACENT)
                {
                    traversedRooms.Clear();
                    visitedRooms.Clear();
                    isReturning = false;
                    if (random.Next(100) < LOST_MOVEMENT_CHANCE)
                    {
                        trader.SetMovementStrategy(MovementStrategyFactory.CreateInstance(MovementStrategies.LOST, trader));
                    }

                    possibleNextRoom = null;
                }
            }
            else
            {
                visitedRooms.Clear();
            }
            return possibleNextRoom;
        }
        else
        {
            HashSet<Room> adjacentRooms = currentRoom.GetAdjacentRooms();
            List<Room> availableRooms = new List<Room>();
            foreach (Room room in adjacentRooms)
            {
                if (!visitedRooms.Contains(room))
                {
                    availableRooms.Add(room);
                }
            }

            if (availableRooms.Count == 0)
            {
                isReturning = true;
                return null;
            }

            possibleNextRoom = availableRooms[random.Next(availableRooms.Count)];
        }
        return possibleNextRoom;
    }
}