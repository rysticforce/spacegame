﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Faction;

public class Trader
{
    private AbstractMovementStrategy movementStrategy;

    private RaceTypes race;

    private Room currentRoom;

    public Trader()
    {
        currentRoom = null;
    }

    public void DoAction()
    {
        movementStrategy.Move(); 
    }

    public void SetMovementStrategy(AbstractMovementStrategy movementStrategy)
    {
        this.movementStrategy = movementStrategy;
    }

    public void SetRace(RaceTypes race)
    {
        this.race = race;
    }

    public void SetCurrentRoom(Room room)
    {
        this.currentRoom = room;
    }

    public Room GetCurrentRoom()
    {
        return currentRoom;
    }
}
