﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static ShipTypes;

public static class FactionAI
{
    private static long totalGameTurns = 0;

    public static void DoTurn(List<Faction> factions)
    {
        totalGameTurns++;
        foreach (Faction faction in factions)
        {
            DoTurnForFaction(faction);
        }
        // do combat
    }

    private static void DoTurnForFaction(Faction faction)
    {
        ShipManager shipManager = faction.GetShipManager();
        ConstructionManager constructionManager = faction.GetConstructionManager();

        shipManager.RemoveMarkedForDestruction();
        shipManager.MoveShips();
        shipManager.AssignOrders();

        constructionManager.DoConstruction();
        constructionManager.AssignNewConstructions();
    }



    public static long GetTotalGameTurns()
    {
        return totalGameTurns; 
    }
}