﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using static ShipTypes;

public class SpaceMain : MonoBehaviour
{
    public Category LILAPOD_START;
    public Category SEIZER_START;
    public Category PLAYER_START;
    public Category NO_TYPE;

    public List<FactionTypes> factionTypes;

    private static readonly System.Random random = new System.Random();

    public GameObject shardfieldTilePrefab;
    public GameObject highlightPrefab;


    private Dictionary<Ship, GameObject> shipObjects;
    private Dictionary<Sector, GameObject> colonizedSectorObjects;

    void Start()
    {
        Debug.Log("started");

        if (GlobalPlayerInfo.mainMap == null)
        {
            GlobalPlayerInfo.mainMap = GenerateShardField();
            GlobalPlayerInfo.factions = GenerateFactions();
        }
            shipObjects = new Dictionary<Ship, GameObject>();
            colonizedSectorObjects = new Dictionary<Sector, GameObject>();
        DrawShardField();
        DrawFactions();
        Debug.Log("finished1");
    }

    static int timer = 0;

    private void Update()
    {
        timer++;
        if (timer >= 2)
        {
            FactionAI.DoTurn(GlobalPlayerInfo.factions);
            timer = 0;
            DrawFactions();
        }
    }

    private List<Faction> GenerateFactions()
    {
        List<Faction> factions = new List<Faction>();
        int width = GlobalPlayerInfo.mainMap.GetLength(0);
        int height = GlobalPlayerInfo.mainMap.GetLength(1);

        foreach (FactionTypes type in factionTypes)
        {
            Faction faction = new Faction(type);
            List<Sector> bestSectors = new List<Sector>();
            int bestScore = 0;

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    int score = 0;
                    Sector currSector = GlobalPlayerInfo.mainMap[i, j];

                    if (currSector.IsColonizedSector())
                        continue;

                    foreach (Shard shard in currSector.GetShards())
                    {
                        if (shard.GetCategory() == faction.GetRacialFavoredShard())
                        {
                            score += shard.GetSize();
                        }

                    }
                    if (score > bestScore)
                    {
                        bestScore = score;
                        bestSectors.Clear();
                        bestSectors.Add(currSector);
                    }
                    else if (score == bestScore)
                    {
                        bestSectors.Add(currSector);
                    }
                }
            }
            int count = random.Next(bestSectors.Count);
            Sector finalSector = bestSectors[count];
            faction.GetExpansionManager().AddColonizedSector(finalSector);
            factions.Add(faction);
        }
        return factions;
    }

    private Sector[,] GenerateShardField()
    {
        int size = 30;
        Sector[,] shardField = new Sector[size, size];

        Sector startA;
        Sector startB;
        Sector startC;
        Sector startD;

        while (true)
        {
            startA = new Sector(random.Next(2) * (size - 1), random.Next(size), 1500, NO_TYPE);
            startB = new Sector(random.Next(size), random.Next(2) * (size - 1), 1500, NO_TYPE);
            startC = new Sector(random.Next(2) * (size - 1), random.Next(size), 1500, NO_TYPE);
            startD = new Sector(random.Next(size), random.Next(2) * (size - 1), 1500, NO_TYPE);

            bool valid =
                (startA.X != startB.X || startA.Y != startB.Y) //
                && Mathf.Abs(startA.X - startB.X) > 5 //
                && Mathf.Abs(startA.Y - startB.Y) > 5;//

            if (valid)
            {
                break;
            }
        }
        Sector startE = new Sector(size / 2, size / 2, 1500, NO_TYPE);

        List<Sector> sectors = new List<Sector>
        {
            startA,
            startB,
            startC,
            startD,
            startE,
        };

        List<CategorySeed> seeds = new List<CategorySeed>
        {
            new CategorySeed(startA, CategoryGenerator.STARTING_STRENGTH / 2, LILAPOD_START),
            new CategorySeed(startB, CategoryGenerator.STARTING_STRENGTH / 2, SEIZER_START),
            new CategorySeed(startC, CategoryGenerator.STARTING_STRENGTH / 2, LILAPOD_START),
            new CategorySeed(startD, CategoryGenerator.STARTING_STRENGTH / 2, SEIZER_START),
            new CategorySeed(startE, CategoryGenerator.STARTING_STRENGTH, PLAYER_START)
        };

        ShardFieldGenerator.CreateShardField(shardField, sectors, NO_TYPE);
        CategoryGenerator.createShardCategories(shardField, seeds);
        return shardField;
    }

    private void DrawShardField()
    {
        int width = GlobalPlayerInfo.mainMap.GetLength(0);
        int height = GlobalPlayerInfo.mainMap.GetLength(1);

        Vector2 cubeSize = new Vector2(.7f, .7f);
        for (float i = 0; i < width; i++)
        {
            for (float j = 0; j < height; j++)
            {

                Sector sector = GlobalPlayerInfo.mainMap[(int)i, (int)j];

                Shard[] shards = sector.GetShards();
                int shardCount = 0;

                float shardFieldOffsetX = i * (.45f * cubeSize.x);
                float shardFieldOffsetY = j * (.45f * cubeSize.y);

                GameObject f2 = Instantiate(highlightPrefab) as GameObject;

                f2.GetComponent<SectorHighlight>().sector = sector;
                f2.transform.localScale = new Vector3(cubeSize.x * 3, cubeSize.y * 3, 0);
                f2.GetComponent<Renderer>().material.color = new Color(0, 0, 0, .25f);
                f2.transform.position = new Vector3(-7.4f + shardFieldOffsetY, -4.55f + shardFieldOffsetX, 2);

                for (float k = 2; k >= 0; k--)
                {
                    for (float l = 0; l < 3; l++)
                    {
                        GameObject f = Instantiate(shardfieldTilePrefab) as GameObject;

                        Shard nextShard = null;
                        if (shards.GetLength(0) > shardCount)
                            nextShard = shards[shardCount];
                        else
                            continue;


                        f.transform.localScale = new Vector3(cubeSize.x, cubeSize.y, 1);
                        f.GetComponent<Renderer>().material.color = nextShard.GetColor();

                        float sectorOffsetX = k * (.15f * cubeSize.x);
                        float sectorOffsetY = l * (.15f * cubeSize.y);

                        f.transform.position = new Vector3(-7.5f + shardFieldOffsetY + sectorOffsetY, -4.65f + shardFieldOffsetX + sectorOffsetX, 0);
                        shardCount++;
                    }
                }
            }
        }
    }

    private void DrawFactions()
    {
        Vector2 cubeSize = new Vector2(.7f, .7f);
        foreach (Faction faction in GlobalPlayerInfo.factions)
        {
            foreach (Sector sector in faction.GetExpansionManager().GetColonizedSectors())
            {
                if (colonizedSectorObjects.ContainsKey(sector))
                {

                }
                else
                {

                    float y = sector.X;
                    float x = sector.Y;

                    float shardFieldOffsetX = y * (.45f * cubeSize.x);
                    float shardFieldOffsetY = x * (.45f * cubeSize.y);

                    GameObject f = Instantiate(shardfieldTilePrefab) as GameObject;
                    f.transform.localScale = new Vector3(cubeSize.x * 3, cubeSize.y * 3, 0);

                    f.transform.position = new Vector3(-7.4f + shardFieldOffsetY, -4.55f + shardFieldOffsetX, -5);
                    f.GetComponent<Renderer>().material.color = faction.GetColor();

                    colonizedSectorObjects.Add(sector, f);
                }
            }

            List<Ship> allShips = new List<Ship>();

            foreach (Ship ship in faction.GetShipManager().GetAllShips())
            {
                float y = ship.GetSector().X;
                float x = ship.GetSector().Y;

                float shardFieldOffsetX = y * (.45f * cubeSize.x);
                float shardFieldOffsetY = x * (.45f * cubeSize.y);

                GameObject f = shipObjects.ContainsKey(ship) ? shipObjects[ship] : Instantiate(shardfieldTilePrefab) as GameObject;

                if (ship.IsMarkedForDestruction())
                {
                    Destroy(f);
                    continue;
                }

                f.transform.localScale = new Vector3(cubeSize.x * 2, cubeSize.y * 2, 0);

                if (ship.GetClassification() == ShipClassification.COLONY)
                {
                    f.transform.localScale = new Vector3(cubeSize.x, cubeSize.y, 0);
                }

                f.transform.position = new Vector3(-7.4f + shardFieldOffsetY, -4.55f + shardFieldOffsetX, -5);
                f.GetComponent<Renderer>().material.color = faction.GetColor();
                if (!shipObjects.ContainsKey(ship))
                    shipObjects.Add(ship, f);
            }
        }
    }
}
