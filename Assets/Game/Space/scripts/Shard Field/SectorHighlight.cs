﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class SectorHighlight : MonoBehaviour
{
    public Sector sector;
    bool hit = false;

    void OnMouseEnter()
    {
        Renderer r = GetComponent<Renderer>();
        r.material.color = new Color(1f, 1f, 0f, .5f);
        hit = true;
    }

    void OnMouseExit()
    {
        Renderer r = GetComponent<Renderer>();
        r.material.color = new Color(0f, 0f, 0f, .25f);
        hit = false;
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0) && hit)
        {
            GlobalPlayerInfo.zoomedSector = sector;
            SceneManager.LoadScene(3);
        }
    }
}