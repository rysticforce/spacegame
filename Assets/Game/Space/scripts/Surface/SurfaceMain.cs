﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class SurfaceMain : MonoBehaviour
{

    private Sector[,] mainMap;

    public Category ELECTRIC_POND;
    public Category LIVING_FACTORY;
    public Category MOON_OF_DESOLATION;
    public Category NO_TYPE;

    public string sceneName;

    private static System.Random random = new System.Random();

    public GameObject surfaceTilePrefab;

    void Start()
    {
        Debug.Log("started");
        DrawShard();
        Debug.Log("finishedee");
    }

    private void DrawShard()
    {
        Shard watchShard = GlobalPlayerInfo.zoomedShard;

        if (watchShard.GetEnvironment() == null)
        {
            EnvironmentGenerator.CreateEnvironment(watchShard);
        }

        int width = watchShard.GetTerrainGrid().GetLength(0);
        int height = watchShard.GetTerrainGrid().GetLength(1);

        SurfaceCell[,] terrainGrid = watchShard.GetTerrainGrid();

        int gapWidth = 0;
        float Xpadding = ((36 - width) / 2) * .425f;
        float Ypadding = ((42 - height) / 2) * .425f;

        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                float xGap = i * gapWidth;
                float yGap = j * gapWidth;

                GameObject f = Instantiate(surfaceTilePrefab) as GameObject;

                float cubesWidth = (i - 1) * .425f;
                float cubesHeight = (j - 1) * .425f;

                f.transform.localScale = new Vector3(3, 3, 5);
                f.GetComponent<Renderer>().material.color = terrainGrid[i, j].terrain.color;
                f.transform.position = new Vector3(-12 + cubesWidth + Xpadding, -8 + cubesHeight + Ypadding);
            }
        }
       // Transform camera = GameObject.Find("MainCamera").transform;
        //camera.position = new Vector3(height / 2, width / 2, -width * 2);


        float curZoomPos, zoomTo = 60f;
        float zoomFrom = 0f;

        // creates a value to raise and lower the camera's field of view
        curZoomPos = zoomFrom + zoomTo;

        curZoomPos = Mathf.Clamp(curZoomPos, 5f, 35f);

        // Stops "zoomTo" value at the nearest and farthest zoom value you desire
        zoomTo = Mathf.Clamp(zoomTo, -15f, 30f);

        // Makes the actual change to Field Of View
        Camera.main.fieldOfView = curZoomPos;

    }

    void Update()
    {
        if (Input.GetMouseButtonDown(1))
        {
            GlobalPlayerInfo.zoomedShard = null;
            SceneManager.LoadScene(3);
        }
    }
}
