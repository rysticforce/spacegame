﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SectorMain : MonoBehaviour
{
    public GameObject shardTilePrefab;

    // Start is called before the first frame update
    void Start()
    {

        Sector displayedSector = GlobalPlayerInfo.zoomedSector;
        Shard[] shards = displayedSector.GetShards();
        int shardCount = 0;
        Debug.Log(shardCount);
        for (float i = 2; i >= 0; i--)
        {
            for (float j = 0; j < 3; j++)
            {

                GameObject f = Instantiate(shardTilePrefab) as GameObject;

                Shard nextShard = null;
                if (shards.GetLength(0) > shardCount)
                    nextShard = shards[shardCount];
                else
                    continue;
                f.GetComponent<ShardHighlight>().shard = nextShard;
                f.GetComponent<Renderer>().material.color = nextShard.GetColor();
                f.transform.localScale = new Vector3(15, 15, 0);
                f.transform.position = new Vector3(-5 + (j * 3), -3 + (i * 3), 0);
                shardCount++;
            }
        }
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(1))
        {
            GlobalPlayerInfo.zoomedSector = null;
            SceneManager.LoadScene(2);
        }
    }
}