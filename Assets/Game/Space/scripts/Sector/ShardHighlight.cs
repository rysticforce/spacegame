﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class ShardHighlight : MonoBehaviour
{
    public Shard shard;
    bool hit = false;

    void OnMouseEnter()
    {
        Renderer r = GetComponent<Renderer>();
        r.material.color = new Color(1f, 1f, 0f, .5f);
        hit = true;
    }

    void OnMouseExit()
    {
        Renderer r = GetComponent<Renderer>();
        r.material.color = shard.GetColor();
        hit = false;
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0) && hit)
        {
            GlobalPlayerInfo.zoomedShard = shard;
            SceneManager.LoadScene(4);
        }
    }
}