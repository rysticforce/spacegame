﻿using System;
using UnityEngine;

public class Shard
{
    public int size;

    private Category category;

    private Environment environment;

    private SurfaceCell[,] terrainGrid;

    public Shard(int size, Category defaultType)
    {
        this.size = size;
        this.category = defaultType;
    }

    public Category GetShardCategory()
    {
        return category;
    }

    public void SetCategory(Category category)
    {
        this.category = category;
    }

    public Category GetCategory()
    {
        return category;
    }

    public void SetEnvironment(Environment environment)
    {
        this.environment = environment;
    }

    public Environment GetEnvironment()
    {
        return environment; 
    }

    public Color GetColor()
    {
        return category.color;
    }

    public void SetTerrainGrid(SurfaceCell[,] terrainGrid)
    {
        this.terrainGrid = terrainGrid;
    }

    public SurfaceCell[,] GetTerrainGrid()
    {
        return terrainGrid;
    }

    public int GetSize()
    {
        return size;
    }
}