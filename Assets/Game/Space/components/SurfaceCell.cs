﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class SurfaceCell
{
    public int X;

    public int Y;

    public Terrain terrain;

    public Terrain previousTerrain;

    public bool isSprawling;

    public ColonizationBlock colonizationBlock;

    private readonly HashSet<SurfaceCell> adjacentCells;

    public SurfaceCell()
    {
        this.adjacentCells = new HashSet<SurfaceCell>();
    }

    public void SetColonizationBlock(ColonizationBlock colonizationBlock)
    {
        this.colonizationBlock = colonizationBlock;
    }

    public void AddAdjacentCell(SurfaceCell cell)
    {
        adjacentCells.Add(cell);
    }

    public HashSet<SurfaceCell> GetAdjacentCells()
    {
        return adjacentCells;
    }
}
