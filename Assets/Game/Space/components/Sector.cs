﻿using System;
using System.Collections;
using System.Collections.Generic;

public class Sector
{
    public static readonly int MINIMUM_SHARD_SIZE = 100;

    public readonly int X;
    public readonly int Y;
    public readonly int totalSizeOfShards;

    private readonly Shard[] shards;

    private static readonly Random random = new Random();
    private Colony colony;

    public Sector(int X, int Y, int size, Category defaultType)
    {
        this.X = X;
        this.Y = Y;

        this.shards = GenerateShards(size, defaultType);
        this.totalSizeOfShards = GetTotalSizeOfShards();
    }

    private Shard[] GenerateShards(int size, Category defaultType)
    {
        List<Shard> createdShards = new List<Shard>();
        int remainingSize = size;

        while (remainingSize > MINIMUM_SHARD_SIZE)
        {
            int shardSize = random.Next(MINIMUM_SHARD_SIZE, remainingSize);
            createdShards.Add(new Shard(shardSize, defaultType));

            remainingSize -= shardSize;
        }
        if (remainingSize > 0)
        {
            createdShards.Add(new Shard(remainingSize, defaultType));
        }

        return createdShards.ToArray();
    }

    private int GetTotalSizeOfShards()
    {
        int totalSize = 0;
        foreach (Shard shard in this.shards)
        {
            totalSize += shard.size;
        }
        return totalSize;
    }

    public bool HasEmptyShard()
    {
        foreach (Shard shard in this.shards)
        {
            if (shard.GetShardCategory().isPassive)
            {
                return true;
            }
        }
        return false;
    }

    public Shard[] GetShards()
    {
        return this.shards;
    }

    // Colony Logic

    public void ColonizeSector(Colony colony)
    {
        this.colony = colony;
    }

    public bool IsColonizedSector()
    {
        return colony != null;
    }

    public Colony GetColony()
    {
        return colony;
    }
}