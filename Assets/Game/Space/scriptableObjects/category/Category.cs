﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using static EnvironmentGenerator;

[CreateAssetMenu(fileName = "New Category Type", menuName = "CategoryTypes")]
public class Category : ScriptableObject
{
    public List<Category> transformsTo;

    public List<Environment> environments;

    public SetupStepName environmentSetup;

    public int startingStrength;

    public Color color;

    public Color otherColor;

    public bool isPassive = false;

}
