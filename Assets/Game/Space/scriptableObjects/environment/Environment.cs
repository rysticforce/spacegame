﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using static EnvironmentGenerator;

[CreateAssetMenu(fileName = "New Environment", menuName = "EnvironmentTypes")]
public class Environment : ScriptableObject
{
    public List<Terrain> terrains;

    public List<GenerationStepName> generationSteps;

    public int cycles;
}
