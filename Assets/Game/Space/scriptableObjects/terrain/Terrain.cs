﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "New Terrain", menuName = "TerrainTypes")]
public class Terrain : ScriptableObject
{
    public Color color;
    public Color sprawlingColor;

    public string collectionName;

    public float heightIncrease;

    public bool highPriority = false;
}
