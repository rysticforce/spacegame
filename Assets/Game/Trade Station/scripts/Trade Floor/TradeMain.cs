﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using static Faction;

public class TradeMain : MonoBehaviour
{
    public GameObject tradeRoomPrefab;

    Trader mainTrader;
    TradeFloor floor;

    void Start()
    {
        Debug.Log("Starting Trade Main");
        floor = new TradeFloor();
        Faction f1 = new Faction(null);
        f1.CreateTraderUnit();
        f1.CreateTraderUnit();

        Faction f2 = new Faction(null);
        f2.CreateTraderUnit();
        f2.CreateTraderUnit();
        Debug.Log("Finishing Trade Main");

        List<Trader> traders = null;// f1.GetTraders();
        traders[0].SetCurrentRoom(floor.GetRooms()[0, 0]);
        mainTrader = traders[0];

        DrawTradeFloor();
    }

    int i1 = 0;

    void Update()
    {
        i1++;
        if (i1 >= 100)
        {
            mainTrader.DoAction();
            i1 = 0;
            //UpdateTradeFloor();
        }
    }

    private void DrawTradeFloor()
    {
        int gapWidth = 0;
        int width = 10;
        int height = 10;
        float Xpadding = ((36 - width) / 2) * .425f;
        float Ypadding = ((42 - height) / 2) * .425f;

        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                float xGap = i * gapWidth;
                float yGap = j * gapWidth;

                GameObject f = Instantiate(tradeRoomPrefab) as GameObject;

                float cubesWidth = (i - 1) * .425f;
                float cubesHeight = (j - 1) * .425f;

                bool isMainRoom = floor.GetRooms()[i,j] == mainTrader.GetCurrentRoom();

                f.transform.localScale = new Vector3(3, 3, 5);
                f.GetComponent<Renderer>().material.color = isMainRoom ? Color.blue : Color.white;
                f.transform.position = new Vector3(-12 + cubesWidth + Xpadding, -8 + cubesHeight + Ypadding);
                f.name = "room" + i + "-" + j;
            }
        }
    }

    private void UpdateTradeFloor()
    {
        int width = 10;
        int height = 10;
        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                bool isMainRoom = floor.GetRooms()[i, j] == mainTrader.GetCurrentRoom();
                GameObject f = GameObject.Find("room" + i + "-" + j);
                f.GetComponent<Renderer>().material.color = isMainRoom ? Color.blue : Color.white;
            }
        }
    }
}
