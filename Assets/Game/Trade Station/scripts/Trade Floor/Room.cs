﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room
{
    public enum Direction
    {
        NORTH,
        SOUTH,
        EAST,
        WEST,
        NOT_ADJACENT
    }

    private Dictionary<Direction, Room> roomDirections;
    private HashSet<Room> adjacentRooms;

    public Room()
    {
        roomDirections = new Dictionary<Direction, Room>();
        adjacentRooms = new HashSet<Room>(); 
    }

    public void SetAdjacentRoom(Room room, Direction direction)
    {
        if (roomDirections.ContainsKey(direction))
        {
            adjacentRooms.Remove(roomDirections[direction]);
        }
        roomDirections[direction] = room;
        adjacentRooms.Add(room);
    }

    public Direction GetDirectionOfAdjacentRoom(Room nextRoom)
    {
        if (roomDirections[Direction.NORTH] == nextRoom)
            return Direction.NORTH;
        if (roomDirections[Direction.SOUTH] == nextRoom)
            return Direction.SOUTH;
        if (roomDirections[Direction.EAST] == nextRoom)
            return Direction.EAST;
        if (roomDirections[Direction.WEST] == nextRoom)
            return Direction.WEST;
        return Direction.NOT_ADJACENT;
    }

    public bool IsRoomAdjacent(Room room)
    {
        return adjacentRooms.Contains(room);
    }

    public HashSet<Room> GetAdjacentRooms()
    {
        return adjacentRooms;
    }
}