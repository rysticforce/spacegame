﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Faction;
using static Room;

public class TradeFloor
{
    private Room[,] rooms;

    public TradeFloor()
    {
        InitRooms();
    }

    private void InitRooms()
    {

        rooms = new Room[10, 10];
        for (int i = 0; i < rooms.GetLength(0); i++)
        {
            for (int j = 0; j < rooms.GetLength(1); j++)
            {
                rooms[i, j] = new Room();
            }
        }

        for (int i = 0; i < rooms.GetLength(0); i++)
        {
            for (int j = 0; j < rooms.GetLength(1); j++)
            {
                if (i < rooms.GetLength(0) - 1)
                {
                    rooms[i, j].SetAdjacentRoom(rooms[i + 1, j], Direction.NORTH);
                }
                if (i > 0)
                {
                    rooms[i, j].SetAdjacentRoom(rooms[i - 1, j], Direction.SOUTH);
                }
                if (j < rooms.GetLength(1) - 1)
                {
                    rooms[i, j].SetAdjacentRoom(rooms[i, j + 1], Direction.EAST);
                }
                if (j > 0)
                {
                    rooms[i, j].SetAdjacentRoom(rooms[i, j - 1], Direction.WEST);
                }
            }
        }

    }

    public Room[,] GetRooms()
    {
        return this.rooms;
    }
}
