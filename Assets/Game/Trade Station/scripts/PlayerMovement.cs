﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class PlayerMovement : MonoBehaviour
{
    private Rigidbody2D body;

    int speed = 5;

    void Start()
    {
        body = GetComponent<Rigidbody2D>();
        body.freezeRotation = true;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        int xVel = Input.GetKey(KeyCode.RightArrow) ? speed : Input.GetKey(KeyCode.LeftArrow) ? -speed : 0;
        int yVel = Input.GetKey(KeyCode.UpArrow) ? speed : Input.GetKey(KeyCode.DownArrow) ? -speed : 0;

        body.velocity = new Vector2(xVel, yVel);
    }

    void OnCollisionEnter (Collision col)
    {

        if (col.gameObject.name == "earthenwareGolem")
        {
            Destroy(col.gameObject);
        }
    }

    private void Form1_Load(object sender, System.EventArgs e)
    {
        Debug.Log("form load");
    }
}
