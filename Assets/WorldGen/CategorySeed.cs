﻿using UnityEngine;
using System.Collections;
using System;

public class CategorySeed
{

    private Sector homeSector;
    private int strength;
    private Category categoryType;
    private int convertedShards;

    private bool completed;

    public CategorySeed(Sector homeSector, int strength, Category shardType)
    {
        this.homeSector = homeSector;
        this.strength = strength;
        this.categoryType = shardType;
        this.completed = false;
        this.convertedShards = 0;
    }

    public void step(Category newShardType)
    {
        bool isCompleted = true;
        foreach (Shard shard in homeSector.GetShards())
        {
            if (shard.GetShardCategory().isPassive)
            {
                shard.SetCategory(newShardType);
                isCompleted = false;
                this.convertedShards++;
                break;
            }
        }
        this.completed = isCompleted;
    }

    public int getX()
    {
        return this.homeSector.X;
    }

    public int getY()
    {
        return this.homeSector.Y;
    }

    public Category getCategoryType()
    {
        return this.categoryType;
    }

    public int getStrength()
    {
        return this.strength;
    }

    public int getConvertedShardCount()
    {
        return this.convertedShards;
    }

    public bool isCompleted()
    {
        return this.completed;
    }
}