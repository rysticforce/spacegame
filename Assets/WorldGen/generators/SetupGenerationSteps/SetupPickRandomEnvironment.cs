﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetupPickRandomEnvironment : ISetupStep
{
    private static readonly System.Random random = new System.Random();

    public Environment Execute(Category category)
    {
        return category.environments[random.Next(category.environments.Count)];
    }
}