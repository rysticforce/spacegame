﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using static Environment;

public static class EnvironmentGenerator
{
    private static readonly Dictionary<SetupStepName, ISetupStep> setupFunctions = new Dictionary<SetupStepName, ISetupStep>();

    private static readonly Dictionary<GenerationStepName, IEnvironmentGenerationStep> genFunctions = new Dictionary<GenerationStepName, IEnvironmentGenerationStep>();

    public enum SetupStepName
    {
        SetupPickRandomEnvironment
    }

    public enum GenerationStepName
    {
        PreCreateBasicTerrainList,
        PreRandomTerrainFromSimilarEnvironment,
        GenGameOfLife,
        PostSmooth
    }

    static EnvironmentGenerator()
    {
        setupFunctions[SetupStepName.SetupPickRandomEnvironment] = new SetupPickRandomEnvironment();

        genFunctions[GenerationStepName.PreCreateBasicTerrainList] = new PreCreateBasicTerrainList();
        genFunctions[GenerationStepName.PreRandomTerrainFromSimilarEnvironment] = new PreRandomTerrainFromSimilarEnvironment();
        genFunctions[GenerationStepName.GenGameOfLife] = new GenGameOfLife();
        genFunctions[GenerationStepName.PostSmooth] = new PostSmooth();
    }

    public static void CreateEnvironment(Shard shard)
    {
        Category category = shard.GetShardCategory();
        List<Terrain> terrains = new List<Terrain>();

        Environment environment = setupFunctions[category.environmentSetup].Execute(category);
        shard.SetEnvironment(environment);

        foreach (GenerationStepName step in environment.generationSteps)
        {
            genFunctions[step].Execute(shard, environment, terrains);
        }
    }
}