﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ShardFieldGenerator : MonoBehaviour
{
    private static System.Random random = new System.Random();

    public static void CreateShardField(Sector[,] shardField, List<Sector> startingSectors, Category defaultType)
    {
        Queue<Sector> qt = new Queue<Sector>();
        foreach (Sector sector in startingSectors)
        {
            shardField[sector.X, sector.Y] = sector;
            qt.Enqueue(sector);
        }

        while (qt.Count > 0)
        {
            Sector current = qt.Dequeue();
            int X = current.X;
            int Y = current.Y;
            int nextSize = current.totalSizeOfShards - Sector.MINIMUM_SHARD_SIZE;

            bool canExpand = true;

            if (nextSize < Sector.MINIMUM_SHARD_SIZE)
            {
                canExpand = false;
                if (random.Next(100) <= 30)
                    nextSize = 300;
                else
                    continue;
            }
            if (X > 0 && shardField[X - 1, Y] == null)
            {
                shardField[X - 1, Y] = new Sector(X - 1, Y, nextSize, defaultType);
                if (canExpand)
                    qt.Enqueue(shardField[X - 1, Y]);
            }

            if (X < shardField.GetLength(0) - 1 && shardField[X + 1, Y] == null)
            {
                shardField[X + 1, Y] = new Sector(X + 1, Y, nextSize, defaultType);
                if (canExpand)
                    qt.Enqueue(shardField[X + 1, Y]);
            }

            if (Y > 0 && shardField[X, Y - 1] == null)
            {
                shardField[X, Y - 1] = new Sector(X, Y - 1, nextSize, defaultType);
                if (canExpand)
                    qt.Enqueue(shardField[X, Y - 1]);
            }
            if (Y < shardField.GetLength(1) - 1 && shardField[X, Y + 1] == null)
            {
                shardField[X, Y + 1] = new Sector(X, Y + 1, nextSize, defaultType);
                if (canExpand)
                    qt.Enqueue(shardField[X, Y + 1]);
            }
        }

        for (int i = 0; i < shardField.GetLength(0); i++)
        {
            for (int j = 0; j < shardField.GetLength(1); j++)
            {
                if (shardField[i,j] == null)
                {
                    shardField[i, j] = new Sector(i, j, 0, defaultType);
                }
            }
        }

    }
}
