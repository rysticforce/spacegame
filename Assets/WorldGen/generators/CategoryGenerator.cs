﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class CategoryGenerator
{
    public static readonly int STARTING_STRENGTH = 15;

    private readonly static System.Random random = new System.Random();

    public static void createShardCategories(Sector[,] shardField, List<CategorySeed> categorySeeds)
    {
        Queue<CategorySeed> qt = new Queue<CategorySeed>();
        foreach (CategorySeed seed in categorySeeds)
        {
            qt.Enqueue(seed);
        }
        while (qt.Count > 0)
        {
            CategorySeed current = qt.Dequeue();
            Category nextPlanetType = DeterminePlanetSeed(current, current.getStrength());
            current.step(nextPlanetType);
            if (current.isCompleted())
            {
                int newStrength = current.getStrength() - current.getConvertedShardCount();

                if (newStrength <= 0)
                {
                    newStrength = 0;
                }

                // determine if the new planet seeds are going to be the same type

                int X = current.getX();
                int Y = current.getY();

                Category nextType;

                if (X > 0 && shardField[X - 1, Y].HasEmptyShard())
                {
                    nextType = DeterminePlanetSeed(current, newStrength);
                    CategorySeed newSeed = new CategorySeed(shardField[X - 1, Y], (nextType == current.getCategoryType() ? newStrength : nextType.startingStrength), nextType);
                    qt.Enqueue(newSeed);
                }

                if (X < shardField.GetLength(0) - 1 && shardField[X + 1, Y].HasEmptyShard())
                {
                    nextType = DeterminePlanetSeed(current, newStrength);
                    CategorySeed newSeed = new CategorySeed(shardField[X + 1, Y], (nextType == current.getCategoryType() ? newStrength : nextType.startingStrength), nextType);
                    qt.Enqueue(newSeed);
                }

                if (Y > 0 && shardField[X, Y - 1].HasEmptyShard())
                {
                    nextType = DeterminePlanetSeed(current, newStrength);
                    CategorySeed newSeed = new CategorySeed(shardField[X, Y - 1], (nextType == current.getCategoryType() ? newStrength : nextType.startingStrength), nextType);
                    qt.Enqueue(newSeed);
                }

                if (Y < shardField.GetLength(1) - 1 && shardField[X, Y + 1].HasEmptyShard())
                {
                    nextType = DeterminePlanetSeed(current, newStrength);
                    CategorySeed newSeed = new CategorySeed(shardField[X, Y + 1], (nextType == current.getCategoryType() ? newStrength : nextType.startingStrength), nextType);
                    qt.Enqueue(newSeed);
                }

            }
            else
            {
                qt.Enqueue(current);
            }
        }
    }

    private static Category DeterminePlanetSeed(CategorySeed seed, int strength)
    {
        int potential = seed.getCategoryType().transformsTo.Count;
        int strengthCheck = random.Next(strength);
        if (strength < potential || strengthCheck < potential)
        {
            Category newType = seed.getCategoryType().transformsTo[strengthCheck];
            return newType;
        }
        return seed.getCategoryType();
    }
}