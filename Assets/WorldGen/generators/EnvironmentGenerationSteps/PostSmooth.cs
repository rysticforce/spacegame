﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PostSmooth : IEnvironmentGenerationStep
{
    public void Execute(Shard shard, Environment environment, List<Terrain> terrains)
    {
        SurfaceCell[,] shardGrid = shard.GetTerrainGrid();
        int gridSize = shardGrid.GetLength(0);
        for (int i = 0; i < gridSize; i++)
        {
            for (int j = 0; j < gridSize; j++)
            {
                Dictionary<Terrain, int> counts = new Dictionary<Terrain, int>();

                Terrain currentTerrain = shardGrid[i, j].terrain;
                Terrain a = i > 0 ? shardGrid[i - 1, j].terrain : shardGrid[gridSize - 1, j].terrain;
                Terrain b = i < gridSize - 1 ? shardGrid[i + 1, j].terrain : shardGrid[0, j].terrain;
                Terrain c = j > 0 ? shardGrid[i, j - 1].terrain : shardGrid[i, gridSize - 1].terrain;
                Terrain d = j < gridSize - 1 ? shardGrid[i, j + 1].terrain : shardGrid[i, 0].terrain;

                counts[a] = counts.ContainsKey(a) ? counts[a] = counts[a] + 1 : counts[a] = 1;
                counts[b] = counts.ContainsKey(b) ? counts[b] = counts[b] + 1 : counts[b] = 1;
                counts[c] = counts.ContainsKey(c) ? counts[c] = counts[c] + 1 : counts[c] = 1;
                counts[d] = counts.ContainsKey(d) ? counts[d] = counts[d] + 1 : counts[d] = 1;

                // This edits the GoL map without making a copy and replacing it. That's intended.

                foreach (Terrain key in counts.Keys)
                {
                    if (counts[key] > 1)
                    {
                        shardGrid[i, j].terrain = key;
                    }
                }
            }
        }
    }
}
