﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenGameOfLife : IEnvironmentGenerationStep
{
    private static readonly System.Random random = new System.Random();

    private static readonly int GENERATIONS_TO_SPRAWLING = 10;

    public void Execute(Shard shard, Environment environment, List<Terrain> terrains)
    {
        Dictionary<Terrain, Terrain> underpopulationValues = new Dictionary<Terrain, Terrain>();
        Dictionary<Terrain, Terrain> overpopulationValues = new Dictionary<Terrain,Terrain>();
        SetUnderAndOverPopulation(underpopulationValues, overpopulationValues, terrains);

        int gridSize = GetGridSize(shard.size);
        shard.SetTerrainGrid(SetStartingTerrainValues(terrains, new SurfaceCell[gridSize, gridSize], gridSize));

        int cycles = environment.cycles;

        for (int i = 0; i < cycles; i++)
        {
            shard.SetTerrainGrid(GameOfLife(shard.GetTerrainGrid(), underpopulationValues, overpopulationValues, terrains));
        }

        WorldGenUtil.SetTerrainGridAdjacency(shard.GetTerrainGrid());
    }

    private int GetGridSize(int maxSize)
    {
        int gridSize = 1;
        while ((gridSize + 1) * (gridSize + 1) < maxSize)
        {
            gridSize++;
        }
        return gridSize;
    }

    private void SetUnderAndOverPopulation(Dictionary<Terrain, Terrain> underpopulationValues, Dictionary<Terrain, Terrain> overpopulationValues, List<Terrain> terrains)
    {
        for (int i = 0; i < terrains.Count; i++)
        {
            underpopulationValues[terrains[i]] = i == 0 ? terrains[terrains.Count - 1] : terrains[i - 1];
            overpopulationValues[terrains[i]] = i == terrains.Count - 1 ? terrains[0] : terrains[i + 1];
        }
    }

    private SurfaceCell[,] SetStartingTerrainValues(List<Terrain> terrains, SurfaceCell[,] shardGrid, int gridSize)
    {
        for (int i = 0; i < gridSize; i++)
        {
            for (int j = 0; j < gridSize; j++)
            {
                shardGrid[i, j] = new SurfaceCell
                {
                    terrain = terrains[random.Next(terrains.Count)]
                };
                shardGrid[i, j].previousTerrain = shardGrid[i, j].terrain;
                shardGrid[i, j].X = i;
                shardGrid[i, j].Y = j;
            }
        }

        return shardGrid;
    }

    private SurfaceCell[,] GameOfLife(SurfaceCell[,] shardGrid, Dictionary<Terrain, Terrain> underpopulationValues, Dictionary<Terrain, Terrain> overpopulationValues, List<Terrain> terrains)
    {

        int gridSize = shardGrid.GetLength(0);

        int remainingSprawlingTiles = (int)(gridSize * gridSize * .3);

        SurfaceCell[,] nextShardGrid = new SurfaceCell[gridSize, gridSize];
        int[,] liveGenerations = new int[gridSize, gridSize];
        for (int i = 0; i < gridSize; i++)
        {
            for (int j = 0; j < gridSize; j++)
            {
                nextShardGrid[i, j] = new SurfaceCell
                {
                    X = i,
                    Y = j
                };

                if (liveGenerations[i, j] >= GENERATIONS_TO_SPRAWLING)
                {
                    nextShardGrid[i, j].terrain = shardGrid[i, j].terrain;
                    if (shardGrid[i, j].isSprawling)
                    {
                        nextShardGrid[i, j].isSprawling = true;
                    }
                    else if (remainingSprawlingTiles > 0)
                    {
                        nextShardGrid[i, j].isSprawling = true;
                        remainingSprawlingTiles--;
                    }
                    continue;
                }

                Terrain currentTerrain = shardGrid[i, j].terrain;
                Terrain a = i > 0 ? shardGrid[i - 1, j].terrain : shardGrid[gridSize - 1, j].terrain;
                Terrain b = i < gridSize - 1 ? shardGrid[i + 1, j].terrain : shardGrid[0, j].terrain;
                Terrain c = j > 0 ? shardGrid[i, j - 1].terrain : shardGrid[i, gridSize - 1].terrain;
                Terrain d = j < gridSize - 1 ? shardGrid[i, j + 1].terrain : shardGrid[i, 0].terrain;

                int sameNeighbors = 0;

                if (a == currentTerrain) sameNeighbors++;
                if (b == currentTerrain) sameNeighbors++;
                if (c == currentTerrain) sameNeighbors++;
                if (d == currentTerrain) sameNeighbors++;

                Terrain nextTerrain = currentTerrain;

                if (sameNeighbors < 2)
                {
                    liveGenerations[i, j] = 0;
                    nextTerrain = underpopulationValues[shardGrid[i, j].terrain];
                }
                else if (sameNeighbors == 4)
                {
                    liveGenerations[i, j] = 0;
                    nextTerrain = overpopulationValues[shardGrid[i, j].terrain];
                }
                else
                {
                    liveGenerations[i, j]++;
                }

                if (nextShardGrid[i, j].previousTerrain == nextTerrain)
                {
                    nextTerrain = terrains[random.Next(terrains.Count)];
                }

                nextShardGrid[i, j].previousTerrain = currentTerrain;
                nextShardGrid[i, j].terrain = nextTerrain;
            }
        }
        return nextShardGrid;
    }
}
