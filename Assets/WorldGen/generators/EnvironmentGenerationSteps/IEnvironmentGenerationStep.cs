﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IEnvironmentGenerationStep
{
    void Execute(Shard shard, Environment environment, List<Terrain> terrains);
}
