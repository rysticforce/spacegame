﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PreCreateBasicTerrainList : IEnvironmentGenerationStep
{
    private static readonly System.Random random = new System.Random();

    public void Execute(Shard shard, Environment environment, List<Terrain> terrains)
    {
        Dictionary<string, List<Terrain>> collections = new Dictionary<string, List<Terrain>>();

        foreach (Terrain terrain in environment.terrains)
        {
            if (!string.IsNullOrEmpty(terrain.collectionName))
            {
                List<Terrain> collection;
                if (!collections.ContainsKey(terrain.collectionName))
                {
                    collections[terrain.collectionName] = new List<Terrain>();
                    collection = collections[terrain.collectionName];
                }
                else
                {
                    collection = collections[terrain.collectionName];
                }
                collection.Add(terrain);
                continue;
            }
            terrains.Add(terrain);
        }

        foreach (string key in collections.Keys)
        {
            List<Terrain> collectionTerrains = collections[key];
            terrains.Add(collectionTerrains[random.Next(collectionTerrains.Count)]);
        }
    }
}