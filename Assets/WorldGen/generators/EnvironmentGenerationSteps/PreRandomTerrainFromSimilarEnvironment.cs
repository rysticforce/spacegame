﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PreRandomTerrainFromSimilarEnvironment : IEnvironmentGenerationStep
{
    private static readonly System.Random random = new System.Random();

    public void Execute(Shard shard, Environment environment, List<Terrain> terrains)
    {
        Category category = shard.GetShardCategory();
        Environment secondEnv = category.environments[random.Next(category.environments.Count)];
        Terrain randomTerrain = secondEnv.terrains[random.Next(secondEnv.terrains.Count)];
        while (randomTerrain.highPriority)
        {
            secondEnv = category.environments[random.Next(category.environments.Count)];
            randomTerrain = secondEnv.terrains[random.Next(secondEnv.terrains.Count)];
        }
        if (!terrains.Contains(randomTerrain))
        {
            terrains.Add(randomTerrain);
        }
    }
}
