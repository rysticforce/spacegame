﻿using System;
public static class WorldGenUtil
{
    public static void SetTerrainGridAdjacency(SurfaceCell[,] terrainGrid)
    {
        for (int i = 0; i < terrainGrid.GetLength(0); i++)
        {
            for (int j = 0; j < terrainGrid.GetLength(1); j++)
            {
                SurfaceCell current = terrainGrid[i, j];
                if (i < terrainGrid.GetLength(0) - 1)
                {
                    current.AddAdjacentCell(terrainGrid[i + 1, j]);
                }

                if (i > 0)
                {
                    current.AddAdjacentCell(terrainGrid[i - 1, j]);
                }

                if (j < terrainGrid.GetLength(1) - 1)
                {
                    current.AddAdjacentCell(terrainGrid[i, j + 1]);
                }

                if (j > 0)
                {
                    current.AddAdjacentCell(terrainGrid[i, j - 1]);
                }
            }
        }
    }
}
